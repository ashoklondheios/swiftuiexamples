//
//  SwfitUIExamplesApp.swift
//  SwfitUIExamples
//
//  Created by Ashok Londhe on 14/05/24.
//

import SwiftUI

@main
struct SwfitUIExamplesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
